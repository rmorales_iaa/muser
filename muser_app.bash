#!/bin/bash
#------------------------------------------------------------------------------
#user variable section
APPLICATION_NAME=muser
#------------------------------------------------------------------------------
#Variable section
UPDATER_JAR=updater-0.1.0-SNAPSHOT.jar
APP_JAR=$APPLICATION_NAME-assembly-0.1.2-SNAPSHOT.jar
#------------------------------------------------------------------------------
#call updater
cd updater
java -jar $UPDATER_JAR
cd ..
#------------------------------------------------------------------------------
#call program
cd app
java -jar $APP_JAR "$@"
#------------------------------------------------------------------------------
#end of file
